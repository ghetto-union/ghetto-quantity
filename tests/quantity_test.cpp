#include <gtest/gtest.h>

#include "quantity.hpp"

using namespace ghetto;
using ArbitraryQuantity = Quantity<1, 1, -2>;
using DimensionlessQuantity = Quantity<0, 0, 0>;
class QuantityTest : public ::testing::Test {
 protected:
  virtual void SetUp() {
    zeroStandard = 0;
    floatStandard = 1.234e-5;
    floatA = -1.23e-3;
    floatB = 0.24e-3;
    floatC = 0.24e-3;
  }
  FloatType zeroStandard;
  FloatType floatStandard;
  FloatType floatA;
  FloatType floatB;
  FloatType floatC;
};

TEST_F(QuantityTest, defaultConstructorGivesZero) {
  DimensionlessQuantity dimensionlessSample;
  EXPECT_EQ(zeroStandard, (FloatType)dimensionlessSample);
  ArbitraryQuantity qtySample;
  EXPECT_EQ(zeroStandard, (FloatType)qtySample);
};

TEST_F(QuantityTest, floatTypeConstructorGivesExactValue) {
  ArbitraryQuantity qtySample(floatStandard);
  EXPECT_EQ(floatStandard, (FloatType)qtySample);
}

TEST_F(QuantityTest, copyConstructorGivesExactValue) {
  ArbitraryQuantity qtyStandard(floatStandard);
  ArbitraryQuantity qtyCopied(qtyStandard);
  EXPECT_EQ(floatStandard, (FloatType)qtyCopied);
  EXPECT_EQ(qtyStandard, qtyCopied);
}

TEST_F(QuantityTest, allowsExplicitCoversion) {
  ArbitraryQuantity qtyStandard(floatStandard);
  // direct initialization
  FloatType f(qtyStandard);
  // explicit conversion
  EXPECT_EQ(f, (FloatType)qtyStandard);
}

TEST_F(QuantityTest, canBeCompared) {
  ArbitraryQuantity qtyA(floatA);
  ArbitraryQuantity qtyB(floatB);
  ArbitraryQuantity qtyC(floatC);
  ASSERT_EQ(qtyA, qtyA);
  ASSERT_NE(qtyA, qtyB);
  ASSERT_LT(qtyA, qtyB);
  ASSERT_LE(qtyA, qtyB);
  ASSERT_LE(qtyB, qtyC);
  ASSERT_GT(qtyB, qtyA);
  ASSERT_GE(qtyB, qtyA);
  ASSERT_GE(qtyC, qtyB);
}

TEST_F(QuantityTest, supportsArithmeticOperationWithoutUnitChange) {
  ArbitraryQuantity qtyA(floatA);
  ArbitraryQuantity qtyB(floatB);
  ArbitraryQuantity qtySum = qtyA + qtyB;
  ArbitraryQuantity qtyDifference = qtyA - qtyB;
  ArbitraryQuantity qtyAmplified = qtyA * floatStandard;
  ArbitraryQuantity qtyAmplifiedCommutative = floatStandard * qtyA;
  ArbitraryQuantity qtyDiminished = qtyA / floatStandard;

  ASSERT_EQ(floatA + floatB, (FloatType)(qtySum));
  ASSERT_EQ(floatA - floatB, (FloatType)(qtyDifference));
  ASSERT_EQ(floatA * floatStandard, (FloatType)(qtyAmplified));
  ASSERT_EQ(floatA * floatStandard, (FloatType)(qtyAmplifiedCommutative));
  ASSERT_EQ(floatA / floatStandard, (FloatType)(qtyDiminished));
}

TEST_F(QuantityTest, unaryArithmeticOperatorIsExact) {
  ArbitraryQuantity qtyStandard(floatStandard);
  ASSERT_EQ(-floatStandard, (FloatType)(-qtyStandard));
  ASSERT_EQ(floatStandard, (FloatType)(+qtyStandard));
}

TEST_F(QuantityTest, supportsArithmeticOperationThatChangesUnit) {
  Quantity<1, 7, -1> qtyA(floatA);
  Quantity<-3, 2, 3> qtyB(floatB);
  Quantity<-2, 9, 2> qtyProduct = qtyA * qtyB;
  Quantity<4, 5, -4> qtyQuotient = qtyA / qtyB;
  ASSERT_EQ(floatA * floatB, (FloatType)qtyProduct);
  ASSERT_EQ(floatA / floatB, (FloatType)qtyQuotient);
}

TEST_F(QuantityTest, canBeExpressedUsingTypeAlias) {
  ForceComponent forceA(floatA);
  Mass massB(floatB);
  AccelerationComponent accelerationAB = forceA / massB;
  ASSERT_EQ(floatA / floatB, (FloatType)accelerationAB);
}

TEST_F(QuantityTest, canBeExpressedUsingUserDefinedLiterals) {
  // Equations of motion (s = ut + 0.5 a * t^2)
  ASSERT_EQ(18.0_m, 4.0_ms_1 * 2.0_s + 0.5 * 5.0_ms_2 * 2.0_s * 2.0_s);
  // Kinetic energy
  ASSERT_EQ(16.0_J, 0.5 * 2.0_kg * 4.0_ms_1 * 4.0_ms_1);
}
