#ifndef GHETTO_DIMENSION_HPP
#define GHETTO_DIMENSION_HPP
#include <array>

namespace ghetto {
static constexpr std::size_t DIMENSIONALITY = 3;
template <class T>
using DimensionProperty = std::array<T, DIMENSIONALITY>;
// underlying basic type of Quantity
using FloatType = float;
using Dimension = DimensionProperty<unsigned int>;
using Stride = Dimension;
using Coordinate = DimensionProperty<int>;
using Point = DimensionProperty<FloatType>;
}  // namespace ghetto

#endif
