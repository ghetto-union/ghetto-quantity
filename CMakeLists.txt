cmake_minimum_required (VERSION 3.2.0)

set(GHETTO_QUANTITY_VERSION_MAJOR "0")
set(GHETTO_QUANTITY_VERSION_MINOR "1")
set(GHETTO_QUANTITY_VERSION_PATCH "3")
set(GHETTO_QUANTITY_VERSION_EXTRA "")
set(GHETTO_QUANTITY_VERSION "${GHETTO_QUANTITY_VERSION_MAJOR}.${GHETTO_QUANTITY_VERSION_MINOR}")
set(GHETTO_QUANTITY_VERSION_FULL "${GHETTO_QUANTITY_VERSION}.${GHETTO_QUANTITY_VERSION_PATCH}${GHETTO_QUANTITY_VERSION_EXTRA}")
set(GHETTO_QUANTITY_LIB_NAME ghetto-quantity)

project(ghetto-quantity
  VERSION ${GHETTO_QUANTITY_VERSION}
  LANGUAGES CXX
)

list(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)
include(ExternalProject)
include(ExternalCMakeProject)

option (ghetto_quantity_with_unit "Differentiate between quantities with different physical units." ON)
option (ghetto_quantity_build_tests "Build ghetto-quantity unit tests." OFF)
if (ghetto_quantity_with_unit)
  add_definitions(-DGHETTO_QUANTITY_WITH_UNIT)
endif()

set(CMAKE_EXPORT_COMPILE_COMMANDS ON CACHE BOOL "" FORCE)

########## dependencies
find_package(Boost REQUIRED)

########## targets
add_library(ghetto-quantity INTERFACE)
add_library(ghetto::ghetto-quantity ALIAS ghetto-quantity)
target_include_directories(ghetto-quantity
  INTERFACE
    $<INSTALL_INTERFACE:include>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    ${Boost_INCLUDE_DIRS}
)

########## install library
include(GNUInstallDirs)
install(TARGETS ghetto-quantity
  EXPORT ghetto-quantity-targets
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
)
install(DIRECTORY include/ghetto
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
)
########## install cmake files
set(INSTALL_CONFIGDIR ${CMAKE_INSTALL_LIBDIR}/cmake/ghetto-quantity)
install(EXPORT ghetto-quantity-targets
  FILE
    ghetto-quantity-targets.cmake
  NAMESPACE
    ghetto::
  DESTINATION
    ${INSTALL_CONFIGDIR}
)
include(CMakePackageConfigHelpers)
write_basic_package_version_file(
  ${CMAKE_CURRENT_BINARY_DIR}/ghetto-quantity-config-version.cmake
  VERSION ${PROJECT_VERSION}
  COMPATIBILITY AnyNewerVersion
)

configure_package_config_file(${CMAKE_CURRENT_LIST_DIR}/cmake/ghetto-quantity-config.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/ghetto-quantity-config.cmake
    INSTALL_DESTINATION ${INSTALL_CONFIGDIR}
)

install(FILES
    ${CMAKE_CURRENT_BINARY_DIR}/ghetto-quantity-config.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/ghetto-quantity-config-version.cmake
    DESTINATION ${INSTALL_CONFIGDIR}
)

########## test
if (ghetto_quantity_build_tests)
  add_subdirectory(tests)
endif()
