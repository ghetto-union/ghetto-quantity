#ifndef GHETTO_QUANTITY_HPP
#define GHETTO_QUANTITY_HPP

#include <boost/operators.hpp>
#include <ostream>
#include "constants.hpp"
#include "dimension.hpp"

namespace ghetto {
// Design Goals:
// 1. All operations supported by Quantity
//    must also be supported by FloatType
//    (The reverse needs not be true),
//    i.e. Quantity mimics FloatType
// 2. Client should always use the aliases

template <int MASS_EXP, int LENGTH_EXP, int TIME_EXP>
class Quantity
    : boost::totally_ordered<Quantity<MASS_EXP, LENGTH_EXP, TIME_EXP>>,
      boost::additive<Quantity<MASS_EXP, LENGTH_EXP, TIME_EXP>>,
      boost::multiplicative2<Quantity<MASS_EXP, LENGTH_EXP, TIME_EXP>,
                             FloatType> {
 public:
  // Constructors
  constexpr Quantity() : scalar(0) {}
  // explicit: only allow direct-initialization, forbids Quantity<> qty = 1.0;
  // explicit Quantity(FloatType v) : scalar(v) {}
  constexpr Quantity(FloatType v) : scalar(v) {}
  // Comparison operator required by totally_ordered<T>
  // Comparison operator required by less_than_comparable<T>
  constexpr bool operator<(const Quantity& q) const {
    return scalar < q.scalar;
  }
  // Comparison operator required by equality_comparable<T>
  constexpr bool operator==(const Quantity& q) const {
    return scalar == q.scalar;
  }

  // Assignment operator required by additive<T>
  // Assignment operator required by addable<T>
  Quantity& operator+=(const Quantity& q) {
    scalar += q.scalar;
    return *this;
  }
  // Assignment operator required by subtractable<T>
  Quantity& operator-=(const Quantity& q) {
    scalar -= q.scalar;
    return *this;
  }

  // Assignment operator required by multiplicative2<T,U>
  // Assignment operator required by multipliable<T,U>
  Quantity& operator*=(const FloatType& f) {
    scalar *= f;
    return *this;
  }
  // Assignment operator required by dividable2<T,U>
  Quantity& operator/=(const FloatType& f) {
    scalar /= f;
    return *this;
  }

  // Arithmetic operators (without change in unit)
  // unary plus (+a)
  constexpr Quantity operator+() const { return Quantity(+scalar); }
  // unary minus (-a)
  constexpr Quantity operator-() const { return Quantity(-scalar); }

  // Arithmetic operators that change sign
  // multiplication
  template <int MASS_EXP1, int LENGTH_EXP1, int TIME_EXP1>
  constexpr Quantity<MASS_EXP + MASS_EXP1, LENGTH_EXP + LENGTH_EXP1,
                     TIME_EXP + TIME_EXP1>
  operator*(const Quantity<MASS_EXP1, LENGTH_EXP1, TIME_EXP1>& q) const {
    return Quantity<MASS_EXP + MASS_EXP1, LENGTH_EXP + LENGTH_EXP1,
                    TIME_EXP + TIME_EXP1>(scalar * static_cast<FloatType>(q));
  }
  // division
  template <int MASS_EXP1, int LENGTH_EXP1, int TIME_EXP1>
  constexpr Quantity<MASS_EXP - MASS_EXP1, LENGTH_EXP - LENGTH_EXP1,
                     TIME_EXP - TIME_EXP1>
  operator/(const Quantity<MASS_EXP1, LENGTH_EXP1, TIME_EXP1>& q) const {
    return Quantity<MASS_EXP - MASS_EXP1, LENGTH_EXP - LENGTH_EXP1,
                    TIME_EXP - TIME_EXP1>(scalar / static_cast<FloatType>(q));
  }

  // misc:
  // sqrt
  // fabs
  // 1/a (reciprocal)

  // Unsupported:
  // - modulo
  // - bitwise operation
  // - logical
  // - increment & decrement
  // - multiplication assignment (*=)
  // - division assignment (/=)

  // User-defined conversion (explicit conversion)
  // Supported operations:
  // 1. direct-initialization (FloatType a(quantity);)&
  // 2. explicit conversions (e.g. static_cast<FloatType>(a), (FloatType)a)ONLY
  explicit constexpr operator FloatType() const { return scalar; }

 private:
  FloatType scalar;
};

// Common physical quantities
#ifdef GHETTO_QUANTITY_WITH_UNIT
using Dimensionless = Quantity<0, 0, 0>;
using Length = Quantity<0, 1, 0>;
using Area = Quantity<0, 2, 0>;
using Volume = Quantity<0, 3, 0>;
using Mass = Quantity<1, 0, 0>;
using Time = Quantity<0, 0, 1>;
using Frequency = Quantity<0, 0, -1>;
using AngularFrequency = Quantity<0, 0, -1>;
using Speed = Quantity<0, 1, -1>;
using AccelerationComponent = Quantity<0, 1, -2>;
using Density = Quantity<1, -3, 0>;
using ForceComponent = Quantity<1, 1, -2>;
using Energy = Quantity<1, 2, -2>;
using MomentumComponent = Quantity<1, 1, -1>;
using Power = Quantity<1, 2, -3>;
using Pressure = Quantity<1, -1, -2>;
#else
using Dimensionless = FloatType;
using Length = FloatType;
using Area = FloatType;
using Volume = FloatType;
using Mass = FloatType;
using Time = FloatType;
using Frequency = FloatType;
using AngularFrequency = FloatType;
using Speed = FloatType;
using AccelerationComponent = FloatType;
using Density = FloatType;
using ForceComponent = FloatType;
using Energy = FloatType;
using MomentumComponent = FloatType;
using Power = FloatType;
using Pressure = FloatType;
#endif

// User-defined floating-point literals
// SI units as base
using FloatLiteralType = long double;

constexpr Dimensionless operator"" _(FloatLiteralType val) {
  return Dimensionless(val);
}

constexpr Dimensionless operator"" _rad(FloatLiteralType val) {
  return Dimensionless(val);
}

constexpr Dimensionless operator"" _deg(FloatLiteralType val) {
  return Dimensionless(val * constants::kPi<FloatLiteralType> / 180.0l);
}

constexpr Length operator"" _m(FloatLiteralType val) { return Length(val); }

constexpr Length operator"" _km(FloatLiteralType val) {
  return Length(val * 1000.0l);
}

constexpr Area operator"" _m2(FloatLiteralType val) { return Area(val); }

constexpr Volume operator"" _m3(FloatLiteralType val) { return Volume(val); }

constexpr Mass operator"" _kg(FloatLiteralType val) { return Mass(val); }

constexpr Time operator"" _s(FloatLiteralType val) { return Time(val); }

constexpr Time operator"" _min(FloatLiteralType val) {
  return Time(val * 60.0l);
}

constexpr Time operator"" _hr(FloatLiteralType val) {
  return Time(val * 60.0l * 60.0l);
}

constexpr Time operator"" _day(FloatLiteralType val) {
  return Time(val * 60.0l * 60.0l * 24.0l);
}

constexpr Frequency operator"" _Hz(FloatLiteralType val) {
  return Frequency(val);
}

constexpr AngularFrequency operator"" _rads_1(FloatLiteralType val) {
  return AngularFrequency(val);
}

constexpr Speed operator"" _ms_1(FloatLiteralType val) { return Speed(val); }

constexpr Speed operator"" _kmhr_1(FloatLiteralType val) {
  return Speed(val * 1000.0l / 60.0l / 60.0l);
}

constexpr AccelerationComponent operator"" _ms_2(FloatLiteralType val) {
  return AccelerationComponent(val);
}

constexpr Density operator"" _kgm_3(FloatLiteralType val) {
  return Density(val);
}

constexpr ForceComponent operator"" _N(FloatLiteralType val) {
  return ForceComponent(val);
}

constexpr Energy operator"" _J(FloatLiteralType val) { return Energy(val); }

constexpr MomentumComponent operator"" _Ns(FloatLiteralType val) {
  return MomentumComponent(val);
}

constexpr Power operator"" _W(FloatLiteralType val) { return Power(val); }

constexpr Pressure operator"" _Pa(FloatLiteralType val) {
  return Pressure(val);
}
#ifdef GHETTO_QUANTITY_WITH_UNIT
template <int MASS_EXP, int LENGTH_EXP, int TIME_EXP>
std::ostream& operator<<(std::ostream& os,
                         const Quantity<MASS_EXP, LENGTH_EXP, TIME_EXP>& q) {
  os << static_cast<FloatType>(q);
  if (MASS_EXP != 0) {
    os << "kg";
    if (MASS_EXP != 1) {
      os << MASS_EXP;
    }
  }
  if (LENGTH_EXP != 0) {
    os << "m";
    if (LENGTH_EXP != 1) {
      os << LENGTH_EXP;
    }
  }
  if (TIME_EXP != 0) {
    os << "s";
    if (TIME_EXP != 1) {
      os << TIME_EXP;
    }
  }
  return os;
}
#endif
}  // namespace ghetto

#endif

