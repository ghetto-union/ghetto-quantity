#include "arithmetic.hpp"
#include <gtest/gtest.h>
#include "tensor.hpp"

using namespace ghetto;

class ArithmeticTest : public ::testing::Test {
 protected:
  ArithmeticTest() {}
  virtual ~ArithmeticTest() {}
};

TEST_F(ArithmeticTest, vectorScale) {
  Position v{1.0_m, -1.3_m, 0.7_m};
  Frequency scale_factor(0.2_Hz);
  Time divisor(0.2_s);
  Velocity scaled = scale_factor * v;
  Velocity divided = v / divisor;
  Velocity expected_scaled{0.2_ms_1, -0.26_ms_1, 0.14_ms_1};
  Velocity expected_divided{5.0_ms_1, -6.5_ms_1, 3.5_ms_1};
  for (std::size_t i = 0; i < expected_scaled.size(); ++i) {
    ASSERT_FLOAT_EQ(static_cast<FloatType>(expected_scaled[i]),
                    static_cast<FloatType>(scaled[i]));
    ASSERT_FLOAT_EQ(static_cast<FloatType>(expected_divided[i]),
                    static_cast<FloatType>(divided[i]));
  }
}

TEST_F(ArithmeticTest, vectorAddition) {
  Position augend{0.5_m, 0.4_m, -0.3_m};
  Position addend{-0.2_m, 0.7_m, 3.2_m};
  Position expected{0.3_m, 1.1_m, 2.9_m};
  augend += addend;
  for (std::size_t i = 0; i < 3; ++i) {
    ASSERT_FLOAT_EQ(static_cast<FloatType>(expected[i]),
                    static_cast<FloatType>(augend[i]));
  }
}

TEST_F(ArithmeticTest, vectorSum) {
  Position p1{0.3, -2.3, 1.3};
  Position p2{2.3, -4.1, 2.3};
  Position sum = p1 + p2;
  Position expected{2.6, -6.4, 3.6};
  for (std::size_t i = 0; i < expected.size(); ++i) {
    ASSERT_FLOAT_EQ(static_cast<FloatType>(expected[i]),
                    static_cast<FloatType>(sum[i]));
  }
}

TEST_F(ArithmeticTest, vectorDifference) {
  Position p1{0.3, -2.3, 1.3};
  Position p2{2.3, -4.1, 2.3};
  Position difference = p1 - p2;
  Position expected{-2.0, 1.8, -1.0};
  for (std::size_t i = 0; i < expected.size(); ++i) {
    ASSERT_FLOAT_EQ(static_cast<FloatType>(expected[i]),
                    static_cast<FloatType>(difference[i]));
  }
}

TEST_F(ArithmeticTest, vectorSubtraction) {
  Position minuend{0.2, 1.3, -2.4};
  Position subtrahend{-0.1, 2.1, 4.2};
  Position expected{0.3, -0.8, -6.6};
  minuend -= subtrahend;

  for (std::size_t i = 0; i < expected.size(); ++i) {
    ASSERT_FLOAT_EQ(static_cast<FloatType>(expected[i]),
                    static_cast<FloatType>(minuend[i]));
  }
}

TEST_F(ArithmeticTest, vectorCrossProduct) {
  Position v1{0.5_m, -1.0_m, 0.125_m};
  Vorticity v2{-2.0_rads_1, -0.5_rads_1, 4.0_rads_1};
  Velocity product(vectorCrossProduct(v1, v2));
  ASSERT_FLOAT_EQ(-3.9375, static_cast<FloatType>(product[0]));
  ASSERT_FLOAT_EQ(-2.25, static_cast<FloatType>(product[1]));
  ASSERT_FLOAT_EQ(-2.25, static_cast<FloatType>(product[2]));
}

TEST_F(ArithmeticTest, pow) {
#ifdef GHETTO_QUANTITY_WITH_UNIT
  Quantity<1, 3, -2> v(2.3);
  Quantity<3, 9, -6> result = pow<3>(v);
  ASSERT_FLOAT_EQ(std::pow(static_cast<FloatType>(v), 3),
                  static_cast<FloatType>(result));
#else
  FloatType v(2.3);
  FloatType result = pow<3>(v);
  ASSERT_FLOAT_EQ(std::pow(v, 3), result);
#endif
}

TEST_F(ArithmeticTest, euclideanDistanceSquared) {
  Position p1{-1.0, 2.0, 3.2};
  Position p2{2.3, -2.1, 1.2};
  Area result = euclideanDistanceSquared(p1, p2);
  ASSERT_FLOAT_EQ(31.7, static_cast<FloatType>(result));
}

TEST_F(ArithmeticTest, normSquared) {
  Position p{-1.3, 0.3, 2.3};
  Area result = normSquared(p);
  ASSERT_FLOAT_EQ(7.07, static_cast<FloatType>(result));
}
