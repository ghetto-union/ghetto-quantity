function(ExternalCMakeProject_Add target)
  include(CMakeParseArguments)
  set(options HEADER_ONLY)
  set(oneValueArgs GIT_REPOSITORY GIT_TAG UPDATE_DISCONNECTED)
  set(multiValueArgs )
  cmake_parse_arguments(ExternalCMakeProject_Add "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
  if (NOT(ExternalCMakeProject_Add_UPDATE_DISCONNECTED EQUAL 0))
    set(ExternalCMakeProject_Add_UPDATE_DISCONNECTED 1)
  endif()

  set(CMAKE_LIST_CONTENT "
    cmake_minimum_required(VERSION 2.8)
    include(ExternalProject)
    ExternalProject_Add(${target}
      PREFIX ${target}
      GIT_REPOSITORY    ${ExternalCMakeProject_Add_GIT_REPOSITORY}
      GIT_TAG           ${ExternalCMakeProject_Add_GIT_TAG}
      UPDATE_DISCONNECTED ${ExternalCMakeProject_Add_UPDATE_DISCONNECTED}
      SOURCE_DIR        ${CMAKE_BINARY_DIR}/${target}/src
      BINARY_DIR        ${CMAKE_BINARY_DIR}/${target}/build
      CONFIGURE_COMMAND \"\"
      BUILD_COMMAND     \"\"
      INSTALL_COMMAND   \"\"
      TEST_COMMAND      \"\"
    )
    ")
  file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/${target}/project)
  file(WRITE ${CMAKE_BINARY_DIR}/${target}/project/CMakeLists.txt ${CMAKE_LIST_CONTENT})
  if(NOT ExternalCMakeProject_Add_HEADER_ONLY)
    execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
      RESULT_VARIABLE result
      WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/${target}/project )
    if(result)
      message(FATAL_ERROR "CMake step for ${target} failed: ${result}")
    endif()
    execute_process(COMMAND ${CMAKE_COMMAND} --build .
      RESULT_VARIABLE result
      WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/${target}/project )
    if(result)
      message(FATAL_ERROR "Build step for ${target} failed: ${result}")
    endif()
    # need to specify binary directory for an out-of-tree source
    add_subdirectory(
      ${CMAKE_BINARY_DIR}/${target}/src
      ${CMAKE_BINARY_DIR}/${target}/build
    )
  endif()
endfunction()
