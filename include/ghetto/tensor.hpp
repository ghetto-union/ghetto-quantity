#ifndef GHETTO_TENSOR_HPP
#define GHETTO_TENSOR_HPP
#include "quantity.hpp"

namespace ghetto {
using Vector = DimensionProperty<Dimensionless>;
using Position = DimensionProperty<Length>;
using Velocity = DimensionProperty<Speed>;
using Momentum = DimensionProperty<MomentumComponent>;
using Force = DimensionProperty<ForceComponent>;
using Acceleration = DimensionProperty<AccelerationComponent>;
using Vorticity = DimensionProperty<AngularFrequency>;

#ifdef GHETTO_QUANTITY_WITH_UNIT
template <std::size_t N, int MASS_EXP, int LENGTH_EXP, int TIME_EXP>
std::ostream& operator<<(
    std::ostream& os,
    std::array<Quantity<MASS_EXP, LENGTH_EXP, TIME_EXP>, N> const& v) {
  os << "{";
  for (std::size_t i = 0; i < N; ++i) {
    if (i != 0) os << ", ";
    os << v[i];
  }
  os << "}";
  return os;
}
#else
template <std::size_t N>
std::ostream& operator<<(std::ostream& os, std::array<FloatType, N> const& v) {
  os << "{";
  for (std::size_t i = 0; i < N; ++i) {
    if (i != 0) os << ", ";
    os << v[i];
  }
  os << "}";
  return os;
}
#endif
}  // namespace ghetto

#endif
