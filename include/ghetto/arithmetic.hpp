#ifndef GHETTO_ARITHMETIC_HPP
#define GHETTO_ARITHMETIC_HPP
#include <array>
#include <cmath>
#include "quantity.hpp"

namespace ghetto {
#ifdef GHETTO_QUANTITY_WITH_UNIT
template <int MASS_EXP, int LENGTH_EXP, int TIME_EXP>
#ifdef _MSC_VER
inline
#else
constexpr
#endif
    Quantity<MASS_EXP / 2, LENGTH_EXP / 2, TIME_EXP / 2>
    sqrt(Quantity<MASS_EXP, LENGTH_EXP, TIME_EXP> const& v) {
  return Quantity<MASS_EXP / 2, LENGTH_EXP / 2, TIME_EXP / 2>(
      std::sqrt(static_cast<FloatType>(v)));
}
#else
#ifdef _MSC_VER
inline
#else
constexpr
#endif
    FloatType
    sqrt(FloatType v) {
  return std::sqrt(v);
}
#endif

#ifdef GHETTO_QUANTITY_WITH_UNIT
template <int POW_EXP, int MASS_EXP, int LENGTH_EXP, int TIME_EXP>
constexpr Quantity<MASS_EXP * POW_EXP, LENGTH_EXP * POW_EXP, TIME_EXP * POW_EXP>
pow(Quantity<MASS_EXP, LENGTH_EXP, TIME_EXP> const& v) {
  return Quantity<MASS_EXP * POW_EXP, LENGTH_EXP * POW_EXP, TIME_EXP * POW_EXP>(
      std::pow(static_cast<FloatType>(v), POW_EXP));
}
#else
template <int POW_EXP>
constexpr FloatType pow(FloatType v) {
  return std::pow(v, POW_EXP);
};
#endif

template <typename T>
int sgn(T v) {
  return (T(0) < v) - (v < T(0));
}

#ifdef GHETTO_QUANTITY_WITH_UNIT
template <int MASS_EXP, int LENGTH_EXP, int TIME_EXP, int MASS_EXP1,
          int LENGTH_EXP1, int TIME_EXP1>
constexpr std::array<Quantity<MASS_EXP + MASS_EXP1, LENGTH_EXP + LENGTH_EXP1,
                              TIME_EXP + TIME_EXP1>,
                     3>
vectorCrossProduct(
    std::array<Quantity<MASS_EXP, LENGTH_EXP, TIME_EXP>, 3> const& multiplicand,
    std::array<Quantity<MASS_EXP1, LENGTH_EXP1, TIME_EXP1>, 3> const&
        multiplier) {
  return {multiplicand[1] * multiplier[2] - multiplicand[2] * multiplier[1],
          multiplicand[2] * multiplier[0] - multiplicand[0] * multiplier[2],
          multiplicand[0] * multiplier[1] - multiplicand[1] * multiplier[0]};
}
#else
template <class T>
constexpr std::array<T, 3> vectorCrossProduct(
    std::array<T, 3> const& multiplicand, std::array<T, 3> const& multiplier) {
  return {multiplicand[1] * multiplier[2] - multiplicand[2] * multiplier[1],
          multiplicand[2] * multiplier[0] - multiplicand[0] * multiplier[2],
          multiplicand[0] * multiplier[1] - multiplicand[1] * multiplier[0]};
}
#endif

template <class T, std::size_t N>
inline std::array<T, N> operator+(std::array<T, N> const& augend,
                                  std::array<T, N> const& addend) {
  std::array<T, N> sum;
  for (std::size_t i = 0; i < N; ++i) {
    sum[i] = augend[i] + addend[i];
  }
  return sum;
}

template <class T, std::size_t N>
inline std::array<T, N> operator-(std::array<T, N> const& minuend,
                                  std::array<T, N> const& subtrahend) {
  std::array<T, N> difference;
  for (std::size_t i = 0; i < N; ++i) {
    difference[i] = minuend[i] - subtrahend[i];
  }
  return difference;
}

#ifdef GHETTO_QUANTITY_WITH_UNIT
template <std::size_t N, int MASS_EXP, int LENGTH_EXP, int TIME_EXP,
          int MASS_EXP1, int LENGTH_EXP1, int TIME_EXP1>
inline std::array<Quantity<MASS_EXP + MASS_EXP1, LENGTH_EXP + LENGTH_EXP1,
                           TIME_EXP + TIME_EXP1>,
                  N>
operator*(std::array<Quantity<MASS_EXP, LENGTH_EXP, TIME_EXP>, N> const& a,
          std::array<Quantity<MASS_EXP1, LENGTH_EXP1, TIME_EXP1>, N> const& b) {
  std::array<Quantity<MASS_EXP + MASS_EXP1, LENGTH_EXP + LENGTH_EXP1,
                      TIME_EXP + TIME_EXP1>,
             N>
      result;
  for (std::size_t i = 0; i < N; ++i) {
    result[i] = a[i] * b[i];
  }
  return result;
}
#else
template <class T1, class T2, std::size_t N>
inline std::array<T2, N> operator*(std::array<T1, N> const& a,
                                   std::array<T2, N> const& b) {
  std::array<T2, N> result;
  for (std::size_t i = 0; i < N; ++i) {
    result[i] = a[i] * b[i];
  }
  return result;
}
#endif

#ifdef GHETTO_QUANTITY_WITH_UNIT
template <std::size_t N, int MASS_EXP_V, int LENGTH_EXP_V, int TIME_EXP_V,
          int MASS_EXP_S, int LENGTH_EXP_S, int TIME_EXP_S>
inline std::array<Quantity<MASS_EXP_V + MASS_EXP_S, LENGTH_EXP_V + LENGTH_EXP_S,
                           TIME_EXP_V + TIME_EXP_S>,
                  N>
operator*(
    Quantity<MASS_EXP_S, LENGTH_EXP_S, TIME_EXP_S> const& scale_factor,
    std::array<Quantity<MASS_EXP_V, LENGTH_EXP_V, TIME_EXP_V>, N> const& v) {
  std::array<Quantity<MASS_EXP_V + MASS_EXP_S, LENGTH_EXP_V + LENGTH_EXP_S,
                      TIME_EXP_V + TIME_EXP_S>,
             N>
      scaled;
  for (std::size_t i = 0; i < N; ++i) {
    scaled[i] = v[i] * scale_factor;
  }
  return scaled;
}
#else
template <class T, std::size_t N>
inline std::array<T, N> operator*(Dimensionless const& scale_factor,
                                  std::array<T, N> const& v) {
  std::array<T, N> scaled;
  for (std::size_t i = 0; i < N; ++i) {
    scaled[i] = v[i] * scale_factor;
  }
  return scaled;
}
#endif

#ifdef GHETTO_QUANTITY_WITH_UNIT
template <std::size_t N, int MASS_EXP_V, int LENGTH_EXP_V, int TIME_EXP_V,
          int MASS_EXP_S, int LENGTH_EXP_S, int TIME_EXP_S>
inline std::array<Quantity<MASS_EXP_V - MASS_EXP_S, LENGTH_EXP_V - LENGTH_EXP_S,
                           TIME_EXP_V - TIME_EXP_S>,
                  N>
operator/(
    std::array<Quantity<MASS_EXP_V, LENGTH_EXP_V, TIME_EXP_V>, N> const& v,
    Quantity<MASS_EXP_S, LENGTH_EXP_S, TIME_EXP_S> const& scale_factor) {
  std::array<Quantity<MASS_EXP_V - MASS_EXP_S, LENGTH_EXP_V - LENGTH_EXP_S,
                      TIME_EXP_V - TIME_EXP_S>,
             N>
      scaled;
  for (std::size_t i = 0; i < N; ++i) {
    scaled[i] = v[i] / scale_factor;
  }
  return scaled;
}
#else
template <class T, std::size_t N>
inline std::array<T, N> operator/(std::array<T, N> const& v,
                                  Dimensionless const& scale_factor) {
  std::array<T, N> scaled;
  for (std::size_t i = 0; i < N; ++i) {
    scaled[i] = v[i] / scale_factor;
  }
  return scaled;
}
#endif

#ifdef GHETTO_QUANTITY_WITH_UNIT
template <std::size_t N, int MASS_EXP_V, int LENGTH_EXP_V, int TIME_EXP_V,
          int MASS_EXP_S, int LENGTH_EXP_S, int TIME_EXP_S>
inline std::array<Quantity<MASS_EXP_S - MASS_EXP_V, LENGTH_EXP_S - LENGTH_EXP_V,
                           TIME_EXP_S - TIME_EXP_V>,
                  N>
operator/(
    Quantity<MASS_EXP_S, LENGTH_EXP_S, TIME_EXP_S> const& scale_factor,
    std::array<Quantity<MASS_EXP_V, LENGTH_EXP_V, TIME_EXP_V>, N> const& v) {
  std::array<Quantity<MASS_EXP_S - MASS_EXP_V, LENGTH_EXP_S - LENGTH_EXP_V,
                      TIME_EXP_S - TIME_EXP_V>,
             N>
      scaled;
  for (std::size_t i = 0; i < N; ++i) {
    scaled[i] = scale_factor / v[i];
  }
  return scaled;
}
#else
template <class T, std::size_t N>
inline std::array<T, N> operator/(Dimensionless const& scale_factor,
                                  std::array<T, N> const& v) {
  std::array<T, N> scaled;
  for (std::size_t i = 0; i < N; ++i) {
    scaled[i] = scale_factor / v[i];
  }
  return scaled;
}
#endif

#ifdef GHETTO_QUANTITY_WITH_UNIT
template <std::size_t N, int MASS_EXP, int LENGTH_EXP, int TIME_EXP,
          int MASS_EXP1, int LENGTH_EXP1, int TIME_EXP1>
inline std::array<Quantity<MASS_EXP + MASS_EXP1, LENGTH_EXP + LENGTH_EXP1,
                           TIME_EXP + TIME_EXP1>,
                  N>
operator/(std::array<Quantity<MASS_EXP, LENGTH_EXP, TIME_EXP>, N> const& a,
          std::array<Quantity<MASS_EXP1, LENGTH_EXP1, TIME_EXP1>, N> const& b) {
  std::array<Quantity<MASS_EXP + MASS_EXP1, LENGTH_EXP + LENGTH_EXP1,
                      TIME_EXP + TIME_EXP1>,
             N>
      result;
  for (std::size_t i = 0; i < N; ++i) {
    result[i] = a[i] / b[i];
  }
  return result;
}
#else
template <class T, std::size_t N>
inline std::array<T, N> operator/(std::array<T, N> const& a,
                                  std::array<T, N> const& b) {
  std::array<T, N> result;
  for (std::size_t i = 0; i < N; ++i) {
    result[i] = a[i] / b[i];
  }
  return result;
}
#endif

#ifdef GHETTO_QUANTITY_WITH_UNIT
template <int MASS_EXP, int LENGTH_EXP, int TIME_EXP>
inline std::array<Quantity<MASS_EXP / 2, LENGTH_EXP / 2, TIME_EXP / 2>, N> sqrt(
    std::array<Quantity<MASS_EXP, LENGTH_EXP, TIME_EXP>, N> const& v) {
  std::array<Quantity<MASS_EXP / 2, LENGTH_EXP / 2, TIME_EXP / 2>, N> result;
  for (std::size_t i = 0; i < N; ++i) {
    result[i] = sqrt(v[i]);
  }
  return result;
}
#else
template <class T, std::size_t N>
inline std::array<T, N> sqrt(std::array<T, N> const& v) {
  std::array<T, N> result;
  for (std::size_t i = 0; i < N; ++i) {
    result[i] = sqrt(v[i]);
  }
  return result;
}
#endif

template <class T, std::size_t N>
inline std::array<int, N> sgn(std::array<T, N> const& v) {
  std::array<int, N> result;
  for (std::size_t i = 0; i < N; ++i) {
    result[i] = sgn(v[i]);
  }
  return result;
}

template <class T, std::size_t N>
inline std::array<T, N>& operator+=(std::array<T, N>& augend,
                                    std::array<T, N> const& addend) {
  for (std::size_t i = 0; i < N; ++i) {
    augend[i] += addend[i];
  }
  return augend;
}

template <class T, std::size_t N>
inline std::array<T, N>& operator-=(std::array<T, N>& minuend,
                                    std::array<T, N> const& subtrahend) {
  for (std::size_t i = 0; i < N; ++i) {
    minuend[i] -= subtrahend[i];
  }
  return minuend;
}

#ifdef GHETTO_QUANTITY_WITH_UNIT
template <std::size_t N, int MASS_EXP, int LENGTH_EXP, int TIME_EXP>
inline Quantity<MASS_EXP * 2, LENGTH_EXP * 2, TIME_EXP * 2> normSquared(
    std::array<Quantity<MASS_EXP, LENGTH_EXP, TIME_EXP>, N> const& v) {
  Quantity<MASS_EXP * 2, LENGTH_EXP * 2, TIME_EXP * 2> accumulated_square(0.0);
  for (std::size_t i = 0; i < N; ++i) {
    accumulated_square += v[i] * v[i];
  }
  return accumulated_square;
}
#else

template <std::size_t N, class T>
inline T normSquared(std::array<T, N> const& v) {
  FloatType accumulated_square(0.0);
  for (std::size_t i = 0; i < N; ++i) {
    accumulated_square += v[i] * v[i];
  }
  return accumulated_square;
}
#endif

template <std::size_t N, class T>
inline T norm(std::array<T, N> const& v) {
  return sqrt(normSquared(v));
}

#ifdef GHETTO_QUANTITY_WITH_UNIT
template <std::size_t N, int MASS_EXP, int LENGTH_EXP, int TIME_EXP>
inline Quantity<MASS_EXP * 2, LENGTH_EXP * 2, TIME_EXP * 2>
euclideanDistanceSquared(
    std::array<Quantity<MASS_EXP, LENGTH_EXP, TIME_EXP>, N> const& p1,
    std::array<Quantity<MASS_EXP, LENGTH_EXP, TIME_EXP>, N> const& p2) {
  Quantity<MASS_EXP * 2, LENGTH_EXP * 2, TIME_EXP * 2> accumulated_square(0.0);
  for (std::size_t i = 0; i < N; ++i) {
    Quantity<MASS_EXP, LENGTH_EXP, TIME_EXP> difference(p1[i] - p2[i]);
    accumulated_square += difference * difference;
  }
  return accumulated_square;
}
#else

template <std::size_t N, class T>
inline T euclideanDistanceSquared(std::array<T, N> const& p1,
                                  std::array<T, N> const& p2) {
  FloatType accumulated_square(0.0);
  for (std::size_t i = 0; i < N; ++i) {
    T difference(p1[i] - p2[i]);
    accumulated_square += difference * difference;
  }
  return accumulated_square;
}
#endif

template <std::size_t N, class T>
#ifdef _MSC_VER
inline
#else
constexpr
#endif
T euclideanDistance(std::array<T, N> const& p1,
                              std::array<T, N> const& p2) {
  return sqrt(euclideanDistanceSquared(p1, p2));
}
}  // namespace ghetto
#endif
